import telebot
# from telebot import types
from telebot.util import quick_markup

with open('token.txt', 'r') as f:
    bot = telebot.TeleBot(f.readline().replace("\n",""))


def create_main_menu(chat):
    markup = quick_markup(
        {
            'Посмотреть фото': {'callback_data': 'my_photo'},
            'О моем хобби': {'callback_data': 'hobby'},
            'Расскажи чего-нибудь': {'callback_data': 'small_talks'},
            'Ссылка на проект': {'url': 'https://gitlab.com/sawyre/test_bot'}
        },
        4
    )

    bot.send_message(chat.id, 'Выберете опцию, либо введите "LOVE":', reply_markup=markup)


def create_small_talks_menu(chat):
    markup = quick_markup(
        {
            'О chatGPT': {'callback_data': 'chatGPT'},
            'О разнице SQL и NoSQL': {'callback_data': 'SQL'},
            'Назад': {'callback_data': 'main_menu'}
        },
        4
    )

    bot.send_message(chat.id, 'О чем рассказать:', reply_markup=markup)


def create_photo_menu(chat):
    markup = quick_markup(
        {
            'Молодость': {'callback_data': 'young_photo'},
            'Старость': {'callback_data': 'old_photo'},
            'Назад': {'callback_data': 'main_menu'}
        },
        4
    )

    bot.send_message(chat.id, 'Какое фото скинуть?', reply_markup=markup)


def send_photo(chat, name):
    photo = open(f'photo/{name}', 'rb')
    bot.send_photo(chat.id, photo)


@bot.message_handler(commands=['start'])
def start(message):
    with open("text/welcome.txt","r") as file:
        reply_message = "\n".join(file.readlines())
        bot.send_message(message.chat.id, reply_message)
    create_main_menu(message.chat)


@bot.callback_query_handler(func=lambda callback: True)
def check_callback_data(callback):
    if callback.data == 'main_menu':
        create_main_menu(callback.message.chat)
    elif callback.data == 'hobby':
        with open("text/about_hobby.txt", "r") as file:
            reply_message = "\n".join(file.readlines())
            bot.send_message(callback.message.chat.id, reply_message)
        create_main_menu(callback.message.chat)
    elif callback.data == 'small_talks':
        create_small_talks_menu(callback.message.chat)
    elif callback.data =='my_photo':
        create_photo_menu(callback.message.chat)
    elif callback.data == 'young_photo':
        send_photo(callback.message.chat, 'young_photo.jpeg')
        create_main_menu(callback.message.chat)
    elif callback.data == 'old_photo':
        send_photo(callback.message.chat, 'old_photo.jpg')
        create_main_menu(callback.message.chat)
    elif callback.data == 'chatGPT':
        voice = open('audio/chatGPT.ogx', 'rb')
        bot.send_voice(callback.message.chat.id, voice)
        create_main_menu(callback.message.chat)
    elif callback.data == 'SQL':
        voice = open('audio/SQL.ogx', 'rb')
        bot.send_voice(callback.message.chat.id, voice)
        create_main_menu(callback.message.chat)


@bot.message_handler(func=lambda message: message.text == 'LOVE')
def send_love(message):
    voice = open('audio/love.ogx', 'rb')
    bot.send_voice(message.chat.id, voice)

bot.infinity_polling()